# -*- coding: utf-8 -*-
from robot import *
import config
import socket
from gtts import gTTS
from chatterbot import ChatBot

# robot instance 
pepper = Pepper(config.IP_ADDRESS, config.PORT)

# UDP socket to communicate with speech recognition
UDP_IP = "127.0.0.1"
UDP_PORT = 5005

sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP
sock.bind((UDP_IP, UDP_PORT))

# Create a new chat bot named Makcik
chatbot = ChatBot(
    'Makcik',
    trainer='chatterbot.trainers.ListTrainer'
)

# chatbot.train([
#     "Hi, can I help you?",
#     "Sure, I'd like to book a flight to Iceland.",
#     "Your flight has been booked."
# ])

chatbot.train([
    "apa khabar",
    "Baik baik aja",
])

chatbot.train([
    "siapa nama kamu",
    "Pepper lah",
])

chatbot.train([
    "dah makan",
    "belum lagi",
])

chatbot.train([
    "hello",
    "hi",
])
###

pepper.say("Hello!")

while True:
	# pepper.say("Give me a question")
	message, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
	print "soalan:", message
	response = chatbot.get_response(message)
	print str(response)
	try:
		# convert the text to an audio file
		tts = gTTS(str(response), lang='id')
		tts.save('speech.mp3')
		print "Uploading..."
		# pepper.upload_file("C:\\Users\\arif\\Documents\\codes\\Pepper\\tmp\\speech.mp3")
		pepper.upload_file('speech.mp3')
		print "Playing sound."
		pepper.play_sound("/home/nao/speech.mp3")
	except Exception as error:
		print(error)
#        pepper.say("I am not sure what to say")
#    vp.say("selamat petang")
#    play_sound(sound):
