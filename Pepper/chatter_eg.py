# -*- coding: utf-8 -*-
from chatterbot import ChatBot

# Create a new chat bot named Charlie
chatbot = ChatBot(
    'Charlie',
    trainer='chatterbot.trainers.ListTrainer'
)

chatbot.train([
    "Hi, can I help you?",
    "Sure, I'd like to book a flight to Iceland.",
    "Your flight has been booked."
])

chatbot.train([
    "apa khabar",
    "Baik baik aja",
])

chatbot.train([
    "siapa nama kamu",
    "Pepper lah",
])

# Get a response to the input text 'How are you?'
response = chatbot.get_response('I would like to book a flight.')
print(response)

response = chatbot.get_response('apa khabar')
print(response)

response = chatbot.get_response('blabla')
print(response)